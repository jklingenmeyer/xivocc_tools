#!/usr/bin/env python
# coding: utf8

import re
import os
import sys
import time
import json
import requests
from xivo_auth_client import Client as Auth

#Information about XiVO API auth
XIVO_INFO = {
    'user': 'user',
    'pass': 'pass'
}

auth = Auth('127.0.0.1', username=XIVO_INFO['user'], password=XIVO_INFO['pass'], verify_certificate=False)
token_data = auth.token.new('xivo_service', expiration=0)
token = token_data['token']
print token

