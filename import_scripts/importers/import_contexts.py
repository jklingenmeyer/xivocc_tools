# coding: utf8

import sys
import traceback
from importers.common import Importer


INTEGERS = ['user_beg', 'user_end', 'group_beg', 'group_end', 'queue_beg', 'queue_end']

class Context(object):
    def __init__(self, **kwargs):
        self.entity = None
        self.display_name = None
        self.description = None
        for key in kwargs:
            self.__dict__[key] = kwargs[key]


class ContextImporter(Importer):

    MANDATORY_PARAMETERS = ['name', 'entity']

    def __init__(self, connection, confd):
        Importer.__init__(self, connection, confd)

    def from_dict(self, context_dict):
        self.empty_strings_to_null(context_dict)
        self._cast_attributes(context_dict)
        return Context(**context_dict)

    def _cast_attributes(self, context_dict):
        for attr in INTEGERS:
            if attr in context_dict and context_dict[attr]:
                context_dict[attr] = int(context_dict[attr])

    def custom_validation(self, context):
        if not self._entity_exists(context.entity):
            raise NoSuchEntityError(context.entity)
        userb = context.__dict__.get('user_beg', None)
        usere = context.__dict__.get('user_end', None)
        groupb = context.__dict__.get('group_beg', None)
        groupe = context.__dict__.get('group_end', None)
        queueb = context.__dict__.get('queue_beg', None)
        queuee = context.__dict__.get('queue_end', None)
        if userb and usere:
            if usere < userb:
                raise InvalidRangeError(context.name)
        if groupb and groupe:
            if groupe < groupb:
                raise InvalidRangeError(context.name)
        if queueb and queuee:
            if queuee < queueb:
                raise InvalidRangeError(context.name)

    def _entity_exists(self, entity):
        cur = self.connection.cursor()
        cur.execute('SELECT id FROM entity WHERE name = %s', (entity,))
        res = cur.fetchone() is not None
        cur.close()
        return res

    def insert(self, context):
        cur = self.connection.cursor()
        try:
            self._insert_table_context(cur, context)
            self._insert_table_contextinclude(cur, context)
            self._insert_table_contextnumbers(cur, context)
            self.connection.commit()
        except Exception:
            self.connection.rollback()
            print 'Erreur inconnue : '
            traceback.print_exc(file=sys.stdout)
        finally:
            cur.close()

    def _insert_table_context(self, cursor, context):
        cursor.execute('''INSERT INTO context(name, displayname, entity, contexttype, commented, description) VALUES
                         (%(name)s, %(display_name)s, %(entity)s, 'internal', 0, %(description)s)''', context.__dict__)

    def _insert_table_contextinclude(self, cursor, context):
        cursor.execute('INSERT INTO contextinclude(context,include,priority) VALUES (%(name)s, \'to_extern\', 0)', context.__dict__)
        cursor.execute('INSERT INTO contextinclude(context,include,priority) VALUES (%(name)s, \'from_extern\', 1)', context.__dict__)

    def _insert_table_contextnumbers(self, cursor, context):
        if context.__dict__.get('user_beg', None):
            cursor.execute('''INSERT INTO contextnumbers(context, type, numberbeg, numberend, didlength) VALUES (%s, 'user', %s, %s, 0)''',
                           (context.name, '%s' % context.user_beg, '%s' % context.user_end))
        if context.__dict__.get('group_beg', None):
            cursor.execute('''INSERT INTO contextnumbers(context, type, numberbeg, numberend, didlength) VALUES (%s, 'group', %s, %s, 0)''',
                           (context.name, '%s' % context.group_beg, '%s' % context.group_end))
        if context.__dict__.get('queue_beg', None):
            cursor.execute('''INSERT INTO contextnumbers(context, type, numberbeg, numberend, didlength) VALUES (%s, 'queue', %s, %s, 0)''',
                           (context.name, '%s' % context.queue_beg, '%s' % context.queue_end))
