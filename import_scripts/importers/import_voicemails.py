# coding: utf8

import os
import sys
import pwd
import grp
import shutil
import requests
import traceback
from importers.common import Importer

class Voicemail(object):
    def __init__(self, **kwargs):
        self.name = None
        self.number = None
        self.context = 'default'
        self.password = None
        self.language = 'fr_FR'
        self.timezone = 'eu-fr'
        self.delete_messages = False
        self.ask_password = True
        self.enabled = True
        for key in kwargs:
            self.__dict__[key] = kwargs[key]


class VoicemailImporter(Importer):
    MANDATORY_PARAMETERS = ['name', 'number']

    def __init__(self, connection, confd):
        Importer.__init__(self, connection, confd)

    def from_dict(self, voicemail_dict):
        self.empty_strings_to_null(voicemail_dict)
        self._extract_options_from_dict(voicemail_dict)
        return Voicemail(**voicemail_dict)
        
    def _extract_options_from_dict(self, voicemail_dict):
        value = voicemail_dict.get('imapuser', None)
        if value:
            voicemail_dict['options'] = [
                [
                      'imapuser',
                      value
                ]
              ]
        del voicemail_dict['imapuser']
        
    def custom_validation(self, voicemail):
        value = voicemail.__dict__.get('imapuser', None)
        if value and '@' not in value:
            raise InvalidParameterError(value, 'de type mail')

    def insert(self, voicemail):
        try:      
                self.confd.voicemails.create(voicemail.__dict__)
        except requests.exceptions.ConnectionError:
                print 'Erreur : Le serveur a refusé la connexion'
        except requests.exceptions.Timeout:
                print 'Erreur : temps d\'attente dépassé'
        except:
                print 'Erreur inconnue : '
                traceback.print_exc(file=sys.stdout)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                print "%s,%s,%s" % (exc_type,exc_value,exc_traceback)
        self._copy_unavail_file(voicemail)
                
    def _copy_unavail_file(self, voicemail):
	rootpath = '/var/spool/asterisk/voicemail/%s/%s' % (voicemail.context, voicemail.number)
	unavail_file_location = '/var/spool/asterisk/voicemail/ag-voicemail-99/7000/unavail.wav'
        try:
                os.makedirs(rootpath + '/tmp')
                os.makedirs(rootpath + '/INBOX')
        except:
                pass
        try:
                shutil.copy2(unavail_file_location, rootpath + '/')
                uid = pwd.getpwnam('asterisk').pw_uid
                gid = grp.getgrnam('asterisk').gr_gid
                for root, dirs, files in os.walk(rootpath):  
                  for i in dirs:  
                    os.chown(os.path.join(root, i), uid, gid)
                  for i in files:
                    os.chown(os.path.join(root, i), uid, gid)
        except:
                print 'Erreur : Impossible de mettre en place le fichier personnalisé d\'indisponibilité unavail.wav pour la mévo %s' % (voicemail.number)
        
