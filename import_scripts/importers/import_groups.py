# coding: utf8

import sys
import traceback
from importers.common import Importer
from importers.exceptions import InvalidForwardError, InvalidParameterError

FORWARD_TRANSLATION = {
    'fail': 'chanunavail',
    'busy': 'busy',
    'noanswer': 'noanswer',
    'congestion': 'congestion'
}

INTEGERS = ['timeout', 'member_timeout', 'retry', 'wrapup', 'maxlen', 'weight', 'schedule_id']

ENUMERATED_ATTRIBUTES = {'autopause': ['yes', 'no'],
                         'strategy': ['ringall', 'wrandom', 'random', 'rrmemory', 'fewestcalls', 'leastrecent', 'linear'],
                         'callerid_mode': ['append', 'prepend', 'overwrite']}


class Group(object):
    def __init__(self, forwards, **kwargs):
        self.context = 'default'
        self.timeout = None
        self.member_timeout = 15
        self.retry = 5
        self.wrapup = 0
        self.maxlen = 0
        self.strategy = 'leastrecent'
        self.weight = 0
        self.autopause = 'no'
        self.musicclass = 'default'

        self.forwards = forwards
        for key in kwargs:
            self.__dict__[key] = kwargs[key]


class Forward(object):
    def __init__(self, type, destination):
        self.type = type
        self.destination = destination

    def __eq__(self, other):
        return (isinstance(other, Forward)
                and self.type == other.type and self.destination == other.destination)

    def __str__(self):
        return 'Forward(%s, %s)' % (self.type, self.destination)


class GroupImporter(Importer):

    MANDATORY_PARAMETERS = ['name', 'number']

    def __init__(self, connection, confd):
        Importer.__init__(self, connection, confd)

    def from_dict(self, group_dict):
        self.empty_strings_to_null(group_dict)
        self._cast_attributes(group_dict)
        return Group(self.extract_forwards_from_dict(group_dict),
                     **group_dict)

    def _cast_attributes(self, group_dict):
        for attr in INTEGERS:
            if attr in group_dict and group_dict[attr]:
                group_dict[attr] = int(group_dict[attr])

    def extract_forwards_from_dict(self, group_dict):
        forwards = []
        keys_to_delete = []
        for key,value in group_dict.iteritems():
            if key.startswith('forward_') and value:
                fwd_name = self.translate_forward(key[len('forward_'):])
                forwards.append(Forward(fwd_name, value))
                keys_to_delete.append(key)
        for key in keys_to_delete:
            del group_dict[key]
        return forwards

    def translate_forward(self, name):
        try:
            return FORWARD_TRANSLATION[name]
        except KeyError:
            raise InvalidForwardError(name)

    def custom_validation(self, group):
        for param, valid_values in ENUMERATED_ATTRIBUTES.iteritems():
            value = group.__dict__.get(param, None)
            if value and value not in valid_values:
                raise InvalidParameterError(value, valid_values)

    def insert(self, group):
        cursor = self.connection.cursor()
        try:
            self._insert_table_groupfeatures(cursor, group)
            self._insert_table_extensions(cursor, group)
            self._insert_table_queue(cursor, group)
            if group.__dict__.get('schedule_id', None):
                self._insert_table_schedule_path(cursor, group)            
            if group.__dict__.get('callerid_mode', None) and group.__dict__.get('callerid_value', None):
                self._insert_table_callerid(cursor, group)
            for forward in group.forwards:
                self._insert_forward(cursor, forward, group.id)
            self._insert_groupmembers(cursor, group)
            if group.__dict__.get('incall_number', None):
		self._insert_incall_table_incall(cursor, group)
                self._insert_incall_table_extensions(cursor, group)
                self._insert_incall_table_dialaction(cursor, group)
            self.connection.commit()
        except Exception:
            self.connection.rollback()
            print 'Erreur inconnue : '
            traceback.print_exc(file=sys.stdout)
        finally:
            cursor.close()

    def _insert_table_groupfeatures(self, cursor, group):
        cursor.execute('''INSERT INTO groupfeatures(name, number, context, ignore_forward, transfer_user, transfer_call, write_caller, write_calling, timeout, preprocess_subroutine)
                VALUES (%(name)s, %(number)s, %(context)s, 1, 1, 0, 0, 0, %(timeout)s, %(preprocess_subroutine)s) RETURNING id''',group.__dict__)
        res = cursor.fetchone()
        group.id = res[0]

    def _insert_table_extensions(self, cursor, group):
        cursor.execute('''INSERT INTO extensions(context, exten, type, typeval) VALUES (%s, %s, 'group', %s)''',
                       (group.context, group.number, '%s' % group.id))

    def _insert_table_queue(self, cursor, group):
        cursor.execute('''INSERT INTO queue(
                name, musicclass, timeout, "queue-youarenext", "queue-thereare", "queue-callswaiting", "queue-holdtime", "queue-minutes",
                "queue-seconds", "queue-thankyou", "queue-reporthold", "periodic-announce", retry, wrapuptime, maxlen, strategy,
                joinempty, leavewhenempty, weight, category, timeoutpriority, autofill, autopause) VALUES (
                %(name)s, %(musicclass)s, %(member_timeout)s, 'queue-youarenext', 'queue-thereare', 'queue-callswaiting', 'queue-holdtime',
                'queue-minutes', 'queue-seconds', 'queue-thankyou', 'queue-reporthold', 'queue-periodic-announce', %(retry)s, %(wrapup)s,
                %(maxlen)s, %(strategy)s, '', '', %(weight)s, 'group', 'app', 0, %(autopause)s)''', group.__dict__)

    def _insert_table_callerid(self, cursor, group):
        cursor.execute('INSERT INTO callerid(mode, callerdisplay, type, typeval) VALUES (%(callerid_mode)s, %(callerid_value)s, \'group\', %(id)s)',
                       group.__dict__)

    def _insert_table_schedule_path(self, cursor, group):
        cursor.execute('INSERT INTO schedule_path(schedule_id, path, pathid, "order") VALUES (%s, \'group\', %s,0)',
                       (group.schedule_id, group.id))
                       
    def _insert_forward(self, cursor, forward, group_id):
        cursor.execute('''INSERT INTO dialaction(event, category, categoryval, action, actionarg1, linked) VALUES
                          (%s, 'group', %s, 'custom', %s, 1)''', (forward.type, '%s' % group_id, forward.destination))

    def _insert_groupmembers(self, cursor, group):
        cursor.execute('SELECT ul.user_id,l.name FROM user_line ul JOIN linefeatures l ON ul.line_id = l.id WHERE context = %s and number not like \'109_\'',
                       (group.context,))
        users = cursor.fetchall()
        for user in users:
            cursor.execute('SELECT coalesce(max(position), 0) FROM queuemember WHERE queue_name = %s', (group.name,))
            position = cursor.fetchone()[0] + 1
            cursor.execute('''INSERT INTO queuemember(queue_name, interface, penalty, usertype, userid, channel, category, position)
                           VALUES (%s, 'SIP/' || %s, 0, 'user', %s, 'SIP', 'group', %s )''',
                           (group.name, user[1], user[0], position))

    def _insert_incall_table_incall(self, cursor, group):
        cursor.execute('''INSERT INTO incall(exten, context, preprocess_subroutine, description)
                VALUES (%(incall_number)s, 'from_extern', %(incall_preprocess_subroutine)s, %(name)s) RETURNING id''',group.__dict__)
        res = cursor.fetchone()
        group.incall_id = res[0]
                               
    def _insert_incall_table_extensions(self, cursor, group):
        cursor.execute('''INSERT INTO extensions(context, exten, type, typeval) VALUES ('from_extern', %s, 'incall', %s)''',
                       (group.incall_number, '%s' % group.incall_id))   
                                           
    def _insert_incall_table_dialaction(self, cursor, group):
        cursor.execute('''INSERT INTO dialaction(event, action, actionarg1, category, categoryval, linked) VALUES ('answer', 'group', %s, 'incall', %s, 1)''',
                       ('%s' % group.id, '%s' % group.incall_id))  
