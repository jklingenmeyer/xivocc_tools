# coding: utf8

import sys
import traceback
from importers.common import Importer
from importers.exceptions import InvalidForwardError, InvalidParameterError

FORWARD_TRANSLATION = {
    'fail': 'chanunavail',
    'busy': 'busy',
    'noanswer': 'noanswer',
    'congestion': 'congestion',
    'max_ewt': 'qwaittime',
    'max_ca_ratio': 'qwaitratio'
}

INTEGERS = ['timeout', 'max_ewt', 'max_ca_ratio', 'member_timeout', 'retry', 'wrapup', 'maxlen', 'weight', 'schedule_id']

ENUMERATED_ATTRIBUTES = {'autopause': ['yes', 'no'],
                         'strategy': ['ringall', 'wrandom', 'random', 'rrmemory', 'fewestcalls', 'leastrecent', 'linear'],
                         'callerid_mode': ['append', 'prepend', 'overwrite']}


class Queue(object):
    def __init__(self, forwards, **kwargs):
        self.context = 'default'
        self.timeout = None
        self.max_ewt = None
        self.max_ca_ratio = None
        self.member_timeout = 15
        self.retry = 5
        self.wrapup = 0
        self.maxlen = 0
        self.strategy = 'leastrecent'
        self.weight = 0
        self.autopause = 'no'
        self.musicclass = 'default'

        self.forwards = forwards
        for key in kwargs:
            self.__dict__[key] = kwargs[key]


class Forward(object):
    def __init__(self, type, destination):
        self.type = type
        self.destination = destination

    def __eq__(self, other):
        return (isinstance(other, Forward)
                and self.type == other.type and self.destination == other.destination)

    def __str__(self):
        return 'Forward(%s, %s)' % (self.type, self.destination)


class QueueImporter(Importer):

    MANDATORY_PARAMETERS = ['name', 'display_name', 'number']

    def __init__(self, connection, confd):
        Importer.__init__(self, connection, confd)

    def from_dict(self, queue_dict):
        self.empty_strings_to_null(queue_dict)
        self._cast_attributes(queue_dict)
        return Queue(self.extract_forwards_from_dict(queue_dict),
                     **queue_dict)

    def _cast_attributes(self, queue_dict):
        for attr in INTEGERS:
            if attr in queue_dict and queue_dict[attr]:
                queue_dict[attr] = int(queue_dict[attr])

    def extract_forwards_from_dict(self, queue_dict):
        forwards = []
        keys_to_delete = []
        for key,value in queue_dict.iteritems():
            if key.startswith('forward_') and value:
                fwd_name = self.translate_forward(key[len('forward_'):])
                forwards.append(Forward(fwd_name, value))
                keys_to_delete.append(key)
        for key in keys_to_delete:
            del queue_dict[key]
        return forwards

    def translate_forward(self, name):
        try:
            return FORWARD_TRANSLATION[name]
        except KeyError:
            raise InvalidForwardError(name)

    def custom_validation(self, queue):
        for param, valid_values in ENUMERATED_ATTRIBUTES.iteritems():
            value = queue.__dict__.get(param, None)
            if value and value not in valid_values:
                raise InvalidParameterError(value, valid_values)

    def insert(self, queue):
        cursor = self.connection.cursor()
        try:
            self._insert_table_queuefeatures(cursor, queue)
            self._insert_table_extensions(cursor, queue)
            self._insert_table_queue(cursor, queue)
            if queue.__dict__.get('schedule_id', None):
                self._insert_table_schedule_path(cursor, queue)
            if queue.__dict__.get('callerid_mode', None) and queue.__dict__.get('callerid_value', None):
                self._insert_table_callerid(cursor, queue)
            for forward in queue.forwards:
                self._insert_forward(cursor, forward, queue.id)
            if queue.__dict__.get('incall_number', None):
		self._insert_incall_table_incall(cursor, queue)
                self._insert_incall_table_extensions(cursor, queue)
                self._insert_incall_table_dialaction(cursor, queue)
            self.connection.commit()
        except Exception:
            self.connection.rollback()
            print 'Erreur inconnue : '
            traceback.print_exc(file=sys.stdout)
        finally:
            cursor.close()

    def _insert_table_queuefeatures(self, cursor, queue):
        cursor.execute('''INSERT INTO queuefeatures(name, displayname, number, context, hitting_caller, transfer_user, timeout, waittime, waitratio, preprocess_subroutine)
                VALUES (%(name)s, %(display_name)s, %(number)s, %(context)s, 1, 1, %(timeout)s, %(max_ewt)s, %(max_ca_ratio)s, %(preprocess_subroutine)s) RETURNING id''',queue.__dict__)
        res = cursor.fetchone()
        queue.id = res[0]

    def _insert_table_extensions(self, cursor, queue):
        cursor.execute('''INSERT INTO extensions(context, exten, type, typeval) VALUES (%s, %s, 'queue', %s)''',
                       (queue.context, queue.number, '%s' % queue.id))
                       
    def _insert_table_queue(self, cursor, queue):
        cursor.execute('''INSERT INTO queue(
                name, musicclass, timeout, "queue-youarenext", "queue-thereare", "queue-callswaiting", "queue-holdtime", "queue-minutes",
                "queue-seconds", "queue-thankyou", "queue-reporthold", "periodic-announce", retry, wrapuptime, maxlen, strategy, "announce-position",
                joinempty, leavewhenempty, weight, category, timeoutpriority, setinterfacevar, setqueueentryvar, setqueuevar, autofill, autopause) VALUES (
                %(name)s, %(musicclass)s, %(member_timeout)s, 'queue-youarenext', 'queue-thereare', 'queue-callswaiting', 'queue-holdtime',
                'queue-minutes', 'queue-seconds', 'queue-thankyou', 'queue-reporthold', 'queue-periodic-announce', %(retry)s, %(wrapup)s,
                %(maxlen)s, %(strategy)s, 'yes', 'inuse,unavailable', '', %(weight)s, 'queue', 'conf', 1, 1, 1, 1, %(autopause)s)''', queue.__dict__)

    def _insert_table_callerid(self, cursor, queue):
        cursor.execute('INSERT INTO callerid(mode, callerdisplay, type, typeval) VALUES (%(callerid_mode)s, %(callerid_value)s, \'queue\', %(id)s)',
                       queue.__dict__)

    def _insert_table_schedule_path(self, cursor, queue):
        cursor.execute('INSERT INTO schedule_path(schedule_id, path, pathid, "order") VALUES (%s, \'queue\', %s,0)',
                       (queue.schedule_id, queue.id))

    def _insert_forward(self, cursor, forward, queue_id):
        cursor.execute('''INSERT INTO dialaction(event, category, categoryval, action, actionarg1, linked) VALUES
                          (%s, 'queue', %s, 'custom', %s, 1)''', (forward.type, '%s' % queue_id, forward.destination))
                          
    def _insert_incall_table_incall(self, cursor, queue):
        cursor.execute('''INSERT INTO incall(exten, context, preprocess_subroutine, description)
                VALUES (%(incall_number)s, 'from_extern', %(incall_preprocess_subroutine)s, %(display_name)s) RETURNING id''',queue.__dict__)
        res = cursor.fetchone()
        queue.incall_id = res[0]
                               
    def _insert_incall_table_extensions(self, cursor, queue):
        cursor.execute('''INSERT INTO extensions(context, exten, type, typeval) VALUES ('from_extern', %s, 'incall', %s)''',
                       (queue.incall_number, '%s' % queue.incall_id))   
                                           
    def _insert_incall_table_dialaction(self, cursor, queue):
        cursor.execute('''INSERT INTO dialaction(event, action, actionarg1, category, categoryval, linked) VALUES ('answer', 'queue', %s, 'incall', %s, 1)''',
                       ('%s' % queue.id, '%s' % queue.incall_id))  
                       
