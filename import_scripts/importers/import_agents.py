# coding: utf8

import sys
import traceback
from importers.common import Importer
from importers.exceptions import NoSuchQueueError

INTEGERS = ['keypos']

class Agent(object):
    def __init__(self, queues, **kwargs):
        self.firstname = None
        self.lastname = None
        self.context = None
        self.number = None
        self.password = None
        self.keypos = 0
        self.queues = queues
        for key in kwargs:
            self.__dict__[key] = kwargs[key]


class AgentImporter(Importer):

    MANDATORY_PARAMETERS = ['firstname', 'lastname', 'number','context']

    def __init__(self, connection, confd):
        Importer.__init__(self, connection, confd)

    def from_dict(self, agent_dict):
	self._cast_attributes(agent_dict)
        self.empty_strings_to_null(agent_dict)
        queues = self._extract_queues_from_dict(agent_dict)
        return Agent(queues, **agent_dict)

    def _cast_attributes(self, agent_dict):
        for attr in INTEGERS:
            if attr in agent_dict and agent_dict[attr]:
                agent_dict[attr] = int(agent_dict[attr])
                
    def _extract_queues_from_dict(self, agent_dict):
        res = []
        try:
            queues_str = agent_dict['queues']
            if queues_str:
                res = queues_str.split(',')
            del agent_dict['queues']
        except KeyError:
            pass
        return res

    def custom_validation(self, agent):
        for queue in agent.queues:
            if not self._queue_exists(queue):
                raise NoSuchQueueError(queue)

    def _queue_exists(self, queue):
        cur = self.connection.cursor()
        cur.execute('SELECT id FROM queuefeatures WHERE name = %s', (queue,))
        res = cur.fetchone() is not None
        cur.close()
        return res

    def _agentgroup_exists(self, agentgroup):
        res = None
        cur = self.connection.cursor()
        cur.execute('SELECT id FROM agentgroup WHERE name = %s and deleted=0', (agentgroup,))
        for row in cur:
            res = row[0]
        cur.close()
        return res

    def insert(self, agent):
        agent.numgroup = self._agentgroup_exists(agent.agentgroup)
        cur = self.connection.cursor()
        try:
            if agent.numgroup is None:
                cur.execute('''INSERT INTO agentgroup(name,groupid) VALUES (%s,currval('agentgroup_id_seq')) RETURNING id''',(agent.agentgroup,))
                agent.numgroup = cur.fetchone()[0]
            cur.execute('''INSERT INTO agentfeatures(context, firstname, lastname, number, passwd, numgroup, language, description)
                           VALUES (%s, %s, %s, %s, %s, %s, 'fr_FR', '') RETURNING id''',
                        (agent.context, agent.firstname, agent.lastname, agent.number, agent.password if agent.password else '', agent.numgroup))
            agent.id = cur.fetchone()[0]
            self._insert_queuemembers(cur, agent)
            self._insert_keydest(cur, agent)
            self._associate_agent_to_eventual_user(cur, agent)
            self.connection.commit()
        except Exception:
            self.connection.rollback()
            print 'Erreur inconnue : '
            traceback.print_exc(file=sys.stdout)
        finally:
            cur.close()
        if agent.user_id:
            self._create_funckey_to_eventual_user(cur, agent)

    def _insert_queuemembers(self, cursor, agent):
        for queue in agent.queues:
            cursor.execute('SELECT coalesce(max(position), 0) FROM queuemember WHERE queue_name = %s', (queue,))
            position = cursor.fetchone()[0] + 1
            cursor.execute('''INSERT INTO queuemember(queue_name, interface, penalty, usertype, userid, channel, category, position)
                           VALUES (%s, 'Agent/' || %s, 0, 'agent', %s, 'Agent', 'queue', %s )''',
                           (queue, agent.number, agent.id, position))
                           
    def _insert_keydest(self, cursor, agent):
		cursor.execute('INSERT INTO func_key(type_id, destination_type_id) VALUES (1, 11) RETURNING id')
		tmp_id = cursor.fetchone()[0]
		cursor.execute('''INSERT INTO func_key_dest_agent(func_key_id, destination_type_id, agent_id, extension_id)
                           VALUES (%s, 11, %s, 1)''', (tmp_id, agent.id))
		cursor.execute('INSERT INTO func_key(type_id, destination_type_id) VALUES (1, 11) RETURNING id')
		tmp_id = cursor.fetchone()[0]
		cursor.execute('''INSERT INTO func_key_dest_agent(func_key_id, destination_type_id, agent_id, extension_id)
                           VALUES (%s, 11, %s, 2)''', (tmp_id, agent.id))
		cursor.execute('INSERT INTO func_key(type_id, destination_type_id) VALUES (1, 11) RETURNING id')
		tmp_id = cursor.fetchone()[0]
		cursor.execute('''INSERT INTO func_key_dest_agent(func_key_id, destination_type_id, agent_id, extension_id)
                           VALUES (%s, 11, %s, 3)''', (tmp_id, agent.id))
		
    def _associate_agent_to_eventual_user(self, cur, agent):
        cur.execute('UPDATE userfeatures u SET agentid = %s FROM user_line ul JOIN linefeatures l ON ul.line_id = l.id WHERE u.id = ul.user_id AND l.name = %s  RETURNING u.id',
                    (agent.id, agent.number))
        res = cur.fetchone()
        if res:
			agent.user_id = int(res[0])
        else:
			agent.user_id = None
        
    def _create_funckey_to_eventual_user(self, cur, agent):
		if agent.keypos > 0:
			try:
					destination = {
						'action': 'toggle',
						'type': 'agent',
						'agent_id': 1
					}
					destination['agent_id'] = int(agent.id)
					funckey = {
						'blf': True,
						'label': 'Réception',
						'destination' : destination
					}
					self.confd.users.relations(agent.user_id).add_funckey(agent.keypos, funckey)
			except requests.exceptions.ConnectionError:
					print 'Erreur : Création de touche - Le serveur a refusé la connexion'
			except requests.exceptions.Timeout:
					print 'Erreur : Création de touche - Délai d\'attente dépassé'
			except:
					print 'Erreur inconnue création de touche : '
					traceback.print_exc(file=sys.stdout)
					exc_type, exc_value, exc_traceback = sys.exc_info()
					print "%s,%s,%s" % (exc_type,exc_value,exc_traceback)
