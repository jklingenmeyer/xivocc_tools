# coding: utf8
import csv
import traceback
import sys
from importers.exceptions import MissingParameterError


class Importer(object):

    MANDATORY_PARAMETERS = []

    def __init__(self, connection, confd):
        self.connection = connection
        self.confd = confd

    def from_dict(self, values):
        pass

    def insert(self, obj):
        pass

    def custom_validation(self, obj):
        pass

    def validate(self, obj):
        for param in self.MANDATORY_PARAMETERS:
            if obj.__dict__.get(param, None) is None:
                raise MissingParameterError(param)
        self.custom_validation(obj)

    def empty_strings_to_null(self, obj_dict):
        for key,value in obj_dict.iteritems():
            if value == '':
                obj_dict[key] = None

    def do_import(self, filename):
        with open(filename, 'r') as csvfile:
            reader = csv.DictReader(csvfile, delimiter='|')
            for line in reader:
                try:
                    obj = self.from_dict(line)
                    self.validate(obj)
                    self.insert(obj)
                except Exception:
                    print 'Erreur d\'import pour la ligne %s' % line
                    traceback.print_exc(file=sys.stdout)
