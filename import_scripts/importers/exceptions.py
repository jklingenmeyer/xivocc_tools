# coding: utf8


class InvalidForwardError(Exception):
    def __init__(self, forward_name):
        Exception.__init__(self, 'Le renvoi de type %s n\'existe pas' % forward_name)


class InvalidParameterError(Exception):
    def __init__(self, parameter, valid_values):
        Exception.__init__(self, 'Seules les valeurs %s sont valides pour le paramètre %s' % (','.join(valid_values), parameter))


class NoSuchQueueError(Exception):
    def __init__(self, queue_name):
        Exception.__init__(self, 'La file d\'attente %s n\'existe pas' % queue_name)

class NoSuchEntityError(Exception):
    def __init__(self, entity_name):
        Exception.__init__(self, 'L\'entité %s n\'existe pas' % entity_name)

class MissingParameterError(Exception):
    def __init__(self, missing_param):
        Exception.__init__(self, 'Le paramètre %s est obligatoire' % missing_param)

class InvalidRangeError(Exception):
    def __init__(self, context_name):
        Exception.__init__(self, 'Mauvais intervalles renseignés pour le contexte %s' % context_name)
