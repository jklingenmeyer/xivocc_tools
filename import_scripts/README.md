Script for importing voicemails, queue, agent, group, agent group and associate user with agent, and user with group
====================================================================

Goal of the script is to import voicemails, queue, agent, group, agent group from CSV file.
It also links user with agent, and user with group.
It also creates a functionnal key on the user's endpoint to log on/off the agent associated to it
It also creates incall rules for groups and queues.
CSV sample files are included.

Features
--------
* importing queues with their incall number if any (in the `from_extern` context)
* importing agents with their incall number if any (in the `from_extern` context)
* importing groups
* importing contexts
* importing voicemails and setting the default unavailable message (unavail.wav)
* associating agent with queue
* associating user with group, all the users within a same context will be included in the group (excepting users with number matching '109X' pattern)
* associating user with agent, if agent number and user sip username match
* creating a func key for the agent to log on/off

Usage
-----
In this order:

1. Create the context
2. Create the users (from another importing tool)
3. Create the group
4. Create the queue
5. Create the agents
6. Create the voicemails

Using the following commands :

```
./main.py context exemple_contextes.csv
./main.py group exemple_groupes.csv
./main.py queue exemple_files.csv
./main.py agent exemple_agents.csv
./main.py voicemail exemple_voicemails.csv
```

Use the provided CSV sample files to create your own ones.

Then

`xivo-service restart all`

...or reload the necessary services (dialplan reload in particular)

What this script DOES NOT
-------------------------

This tool cannot be used to :

* create new users : use the integrated XiVO import tool
* create new endpoints : use the `add-device-by-mac.py` script
* create new lines : use the integrated XiVO import tool and the `edit_users-add-device-by-mac.py` script
