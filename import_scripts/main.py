#!/usr/bin/python
import psycopg2
import sys
from xivo_auth_client import Client as Auth
from xivo_confd_client import Client as Confd
from importers.import_contexts import ContextImporter
from importers.import_agents import AgentImporter
from importers.import_groups import GroupImporter
from importers.import_queues import QueueImporter
from importers.import_voicemails import VoicemailImporter

DB_CONNECTION_PARAMS = {'host': 'localhost',
                     'user': 'asterisk',
                     'password': 'proformatique',
                     'database': 'asterisk'}

API_CONNECTION_PARAMS = {'user': 'sendmail',
                     'password': 'superpass'}

IMPORTER_CLASSES = {'context': ContextImporter,
                    'agent': AgentImporter,
                    'group': GroupImporter,
                    'queue': QueueImporter,
                    'voicemail': VoicemailImporter}


def main():
    if len(sys.argv) != 3:
        print 'Usage : %s context|agent|group|queue|voicemail csv_file' % sys.argv[0]
        exit(1)
    importer_name = sys.argv[1]
    filename = sys.argv[2]
    connection = psycopg2.connect(**DB_CONNECTION_PARAMS)
    auth = Auth('127.0.0.1', username=API_CONNECTION_PARAMS['user'], password=API_CONNECTION_PARAMS['password'], verify_certificate=False)
    token_data = auth.token.new('xivo_service', expiration=10)
    token = token_data['token']
    confd = Confd('127.0.0.1', port=9486, verify_certificate=False, token=token)
    IMPORTER_CLASSES[importer_name](connection, confd).do_import(filename)


if __name__ == '__main__':
    main()
