#!/bin/bash

COMPOSE_CONTAINER_IDS=$(docker ps -a --format "{{.ID}}" --filter "name=compose_")
COMPOSE_CONTAINER_NAMES=$(docker ps -a --format "{{.Names}}" --filter "name=compose_")


echo "The following containers will be disabled: $COMPOSE_CONTAINER_NAMES"


echo "Status of these containers BEFORE deactivation : "
for i in $COMPOSE_CONTAINER_IDS ; do
    docker inspect ${i} | grep -A3 RestartPolicy | grep ame
done

echo "Deactivation..."
docker update --restart=no $COMPOSE_CONTAINER_IDS 2>&1 > /dev/null

echo "...done."
echo "Status of these containers AFTER deactivation : "
for i in $COMPOSE_CONTAINER_IDS ; do
    docker inspect ${i} | grep -A3 RestartPolicy | grep ame
done

echo "Did it work ?"
