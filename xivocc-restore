#!/bin/bash
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# version   = 2
# date      = 2017-08-25 17:27:49 (UTC+0200)
# copyright = 'Copyright (C) 2017 Avencall'
# author    = 'jklingenmeyer@xivo.solutions'

# Standard color
RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
NOCOLOR='\033[0m'

if [ -z $1 ]; then
    echo "Required argument missing: backup type"
    usage
fi

if [ -z $2 ]; then
    echo "Required argument missing: backup file"
    usage
fi

PATH=/usr/sbin:/usr/bin:/sbin:/bin
RESTORE_TYPE=$1
RESTORE_FILE=$2

renice 19 "$$" >/dev/null
ionice -c 3 -p "$$"

RESTORE_FILE=${RESTORE_FILE}

function usage {
    echo "Usage: xivocc-restore <backup-type> <backup-file>"
    echo "  <backup-type>: data|db|db-stats|db-config|db-recording|db-spagobi"
    exit -1
}

#check if docker exists, if exists, set the variable DOCKER_NAME withe the container name
function check_docker
{
        echo -ne "Checking if ${1} exists...\t\t"
	DOCKER_NAME=$(docker ps -a --format='{{.Names}}' | grep ${1})
        if [ -z "$DOCKER_NAME" ]; then
            echo -e "[${RED}NOT FOUND${NOCOLOR}"
            exit -1
        else
            echo -e "[${GREEN}OK${NOCOLOR}]"
        fi

}

function check_container_state {
    CONTAINER=$1
    echo -ne "Checking ${CONTAINER} state...\t"
    CONTAINER_STATE=$(docker inspect --format '{{.State.Status}}' ${CONTAINER} 2>/dev/null)
    if [ -z "$CONTAINER_STATE" ]; then
        echo -e "[${RED}KO: NOT FOUND${NOCOLOR}]"
        exit -1
    elif [ "$CONTAINER_STATE" != "running" ]; then
        echo -e "[${RED}KO: '${CONTAINER_STATE}' - SHOULD BE 'running'${NOCOLOR}]"
        exit -1
    else
        echo -e "[${GREEN}OK${NOCOLOR}]"
    fi
}

function restore_pgxivocc_database
{
	DOCKER_PGXIVOCC_NAME=$1
	DOCKER_DATABASE_NAME=$2
	TMPDIR=$3
	PG_DIR=pg-backup
	PG_TMPDIR=${TMPDIR}/${PG_DIR}
        PG_TMPDUMPFILE=${PG_TMPDIR}/${DOCKER_DATABASE_NAME}_dump
	if [ ! -f "${PG_TMPDUMPFILE}" ]; then
                echo -e "${RED}Unable to find dump file from extract. Aborting.${NOCOLOR}"
        else
        	cd ${PG_TMPDIR}
                docker run --rm --link ${DOCKER_PGXIVOCC_NAME}:db -v $(pwd):/pg-backup -e PGPASSWORD=xivocc xivoxc/pgxivocc pg_restore -h db -c -U postgres --format=c -d ${DOCKER_DATABASE_NAME} pg-backup/${DOCKER_DATABASE_NAME}_dump
        	cd ${OLDPWD}
                echo -e "[${GREEN}Done${NOCOLOR}]"
	fi
}

case "${RESTORE_TYPE}" in
	data)
                echo -e "\nYou are about to ${YELLOW}completely${NOCOLOR} restore files from ${YELLOW}$RESTORE_FILE${NOCOLOR},"
                echo -e "${YELLOW}This action cannot be undone!${NOCOLOR}"
                read -p "Are you sure you want to continue? (y/N): " -n 1 -r
                
                if [[ ! $REPLY =~ ^[Yy]$ ]]; then
                    echo -e "\nAborting"
                    exit -1
                fi
		tar xvpf ${RESTORE_FILE} -C / >/dev/null
		;;
	db-stats)
		check_docker pgxivocc
                check_container_state ${DOCKER_NAME}

                echo -e "\nYou are about to restore ${YELLOW}Reporting (xivo_stats)${NOCOLOR} DB data from ${YELLOW}$RESTORE_FILE${NOCOLOR},"
                echo -e "${YELLOW}This action cannot be undone and current data will be lost!${NOCOLOR}"
                read -p "Are you sure you want to continue? (y/N): " -n 1 -r
                
                if [[ ! $REPLY =~ ^[Yy]$ ]]; then
                    echo -e "\nAborting"
                    exit -1
                fi
                echo -e "\nRestore is starting:"
	        TMPDIR=$(mktemp -d)
                echo -ne " * Extracting tar file...\t"
	        tar xpzf ${RESTORE_FILE} -C ${TMPDIR}
                echo -e "[${GREEN}Done${NOCOLOR}]"
                echo -ne " * Restoring spagobi DB...\t"
	        restore_pgxivocc_database ${DOCKER_NAME} xivo_stats ${TMPDIR}
	        rm -r ${TMPDIR}
		;;
	db-config)
		check_docker pgxivocc
                check_container_state ${DOCKER_NAME}

                echo -e "\nYou are about to restore ${YELLOW}XuC Rights${NOCOLOR} DB data from ${YELLOW}$RESTORE_FILE${NOCOLOR},"
                echo -e "${YELLOW}This action cannot be undone and current data will be lost!${NOCOLOR}"
                read -p "Are you sure you want to continue? (y/N): " -n 1 -r
                
                if [[ ! $REPLY =~ ^[Yy]$ ]]; then
                    echo -e "\nAborting"
                    exit -1
                fi
                echo -e "\nRestore is starting:"
	        TMPDIR=$(mktemp -d)
                echo -ne " * Extracting tar file...\t"
	        tar xpzf ${RESTORE_FILE} -C ${TMPDIR}
                echo -e "[${GREEN}Done${NOCOLOR}]"
                echo -ne " * Restoring spagobi DB...\t"
	        restore_pgxivocc_database ${DOCKER_NAME} xuc_rights ${TMPDIR}
	        rm -r ${TMPDIR}
		;;
	db-recording)
		check_docker pgxivocc
                check_container_state ${DOCKER_NAME}

                echo -e "\nYou are about to restore ${YELLOW}recording${NOCOLOR} DB data from ${YELLOW}$RESTORE_FILE${NOCOLOR},"
                echo -e "${YELLOW}This action cannot be undone and current data will be lost!${NOCOLOR}"
                read -p "Are you sure you want to continue? (y/N): " -n 1 -r
                
                if [[ ! $REPLY =~ ^[Yy]$ ]]; then
                    echo -e "\nAborting"
                    exit -1
                fi
                echo -e "\nRestore is starting:"
	        TMPDIR=$(mktemp -d)
                echo -ne " * Extracting tar file...\t"
	        tar xpzf ${RESTORE_FILE} -C ${TMPDIR}
                echo -e "[${GREEN}Done${NOCOLOR}]"
                echo -ne " * Restoring spagobi DB...\t"
	        restore_pgxivocc_database ${DOCKER_NAME} recording ${TMPDIR}
	        rm -r ${TMPDIR}
		;;
	db-spagobi)
		check_docker pgxivocc
                check_container_state ${DOCKER_NAME}

                echo -e "\nYou are about to restore ${YELLOW}SpagoBI${NOCOLOR} DB data from ${YELLOW}$RESTORE_FILE${NOCOLOR},"
                echo -e "${YELLOW}This action cannot be undone and current data will be lost!${NOCOLOR}"
                read -p "Are you sure you want to continue? (y/N): " -n 1 -r
                
                if [[ ! $REPLY =~ ^[Yy]$ ]]; then
                    echo -e "\nAborting"
                    exit -1
                fi
                echo -e "\nRestore is starting:"
	        TMPDIR=$(mktemp -d)
                echo -ne " * Extracting tar file...\t"
	        tar xpzf ${RESTORE_FILE} -C ${TMPDIR}
                echo -e "[${GREEN}Done${NOCOLOR}]"
                echo -ne " * Restoring spagobi DB...\t"
	        restore_pgxivocc_database ${DOCKER_NAME} spagobi ${TMPDIR}
	        rm -r ${TMPDIR}
		;;
	db)
		check_docker pgxivocc
                check_container_state ${DOCKER_NAME}

                echo -e "\nYou are about to ${YELLOW}completely${NOCOLOR} restore DB data from ${YELLOW}$RESTORE_FILE${NOCOLOR},"
                echo -e "${YELLOW}This action cannot be undone and current data will be lost!${NOCOLOR}"
                read -p "Are you sure you want to continue? (y/N): " -n 1 -r
                
                if [[ ! $REPLY =~ ^[Yy]$ ]]; then
                    echo -e "\nAborting"
                    exit -1
                fi
                echo -e "\nRestore is starting:"
	        TMPDIR=$(mktemp -d)
                echo -ne " * Extracting tar file...\t"
	        tar xpzf ${RESTORE_FILE} -C ${TMPDIR}
                echo -e "[${GREEN}Done${NOCOLOR}]"
                echo -ne " * Restoring xuc_rights DB...\t"
	        restore_pgxivocc_database ${DOCKER_NAME} xuc_rights ${TMPDIR}
                echo -ne " * Restoring recording DB...\t"
	        restore_pgxivocc_database ${DOCKER_NAME} recording ${TMPDIR}
                echo -ne " * Restoring spagobi DB...\t"
	        restore_pgxivocc_database ${DOCKER_NAME} spagobi ${TMPDIR}
	        rm -r ${TMPDIR}
		;;
	kibana)
		check_docker kibana
                echo -e "\nYou are about to restore ${YELLOW}Kibana panels${NOCOLOR} from ${YELLOW}$RESTORE_FILE${NOCOLOR},"
                echo -e "${YELLOW}This action cannot be undone and current data will be lost!${NOCOLOR}"
                read -p "Are you sure you want to continue? (y/N): " -n 1 -r
                
                if [[ ! $REPLY =~ ^[Yy]$ ]]; then
                    echo -e "\nAborting"
                    exit -1
                fi
                echo -e "\nRestore is starting:"
		TMPDIR=$(mktemp -d)
                tar xpzf ${RESTORE_FILE} -C ${TMPDIR}
                KIBANA_TMPDIR=${TMPDIR}/kibana
#		mkdir ${TMPDIR}/kibana
#		KIBANA_TMP=${TMPDIR}/kibana/
#		python /usr/sbin/import-kibana-dashboards.py ${RESTORE_FILE}
		rm -r ${TMPDIR}
		;;
  	*)
    		echo "Unknown backup type" >&2
		exit 1
		;;
esac


        PG_TMPDUMPFILE=${PG_TMPDIR}/${DOCKER_DATABASE_NAME}_dump
